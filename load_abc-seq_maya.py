# author: mahesh ms
# contact: msmahesh@live.com
# Date: 6-10-2018

import maya.cmds as cmds


def load_abc():
    # setup alembic reference with a namespace 'water'
    fname = "C:/abc_seq/water/water.1.abc"
    rname = "water"

    try:
        cmds.select('water' + ':*')
        print 'EFX Data already loaded!'
    except:
        cmds.file(fname, r=True, ignoreVersion=True, namespace=rname)
        # python code for pre frame mel script
        py_code = '''python("import maya.cmds as cmds\\ntime = cmds.currentTime(q=True)\\nd=str(int(time))\\nfname='C:/abc_seq/water/water.%s.abc'%d\\ncmds.file(fname, loadReference='waterRN')");'''

        # set pre frame mel script
        cmds.setAttr('defaultRenderGlobals.preRenderMel', py_code, type='string')
        #cmds.setAttr('defaultRenderGlobals.postRenderMel', py_code, type='string')

        # python code for viewport update
        myCode = '''
import maya.cmds as cmds

def on_time_changed():
    time = cmds.currentTime(q=True)
    d = str(int(time))
    fname = "C:/abc_seq/water/water.%s.abc" %d
    cmds.file(fname, loadReference="waterRN")
on_time_changed()
cmds.scriptJob(event=["timeChanged", "on_time_changed()"])
cmds.scriptJob(event=["SceneOpened", "on_time_changed()"])
on_time_changed()
'''

        # create a script node and add mycode
        nodeName = cmds.scriptNode(st=1, bs=myCode.replace("'''", "''"), n='sn_abcChange', stp='python')
        # run script node
        cmds.scriptNode(nodeName, executeBefore=True)


# run the function load_abc()
load_abc()
